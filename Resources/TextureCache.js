"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TextureCache = (function () {
    function TextureCache() {
        this._textures = new Map();
    }
    TextureCache.prototype.addToCache = function (texture) {
        this._textures.set(texture.name, texture);
    };
    TextureCache.prototype.get = function (name) {
        return this._textures.get(name);
    };
    TextureCache.getInstance = function () {
        return this._instance;
    };
    TextureCache._instance = new TextureCache();
    return TextureCache;
}());
exports.TextureCache = TextureCache;
//# sourceMappingURL=TextureCache.js.map