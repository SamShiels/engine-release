import { ITexture } from "./ITexture";
export declare class TextureCache {
    private static _instance;
    private _textures;
    private constructor();
    addToCache(texture: ITexture): void;
    get(name: string): ITexture | undefined;
    static getInstance(): TextureCache;
}
