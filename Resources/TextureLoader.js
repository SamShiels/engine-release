"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var TextureCache_1 = require("./TextureCache");
var TextureLoader = (function () {
    function TextureLoader() {
    }
    TextureLoader.load = function (imageUrl, jsonUrl) {
        return __awaiter(this, void 0, void 0, function () {
            var htmlImage, texturesToCache, cacheHackDownloadTries, loadImagePromise, jsonReq, loadJsonPromise;
            return __generator(this, function (_a) {
                htmlImage = new Image();
                htmlImage.src = imageUrl;
                texturesToCache = [];
                cacheHackDownloadTries = 0;
                loadImagePromise = new Promise(function (resolve, reject) {
                    htmlImage.onload = function () {
                        cacheHackDownloadTries++;
                        if (cacheHackDownloadTries === 1) {
                            htmlImage.src = imageUrl;
                        }
                        else if (cacheHackDownloadTries === 2) {
                            resolve();
                        }
                    };
                    htmlImage.onerror = function () {
                        reject();
                    };
                });
                jsonReq = new XMLHttpRequest();
                jsonReq.overrideMimeType("application/json");
                if (jsonUrl !== undefined) {
                    jsonReq.open('GET', jsonUrl, true);
                    jsonReq.send();
                }
                else {
                    texturesToCache.push({
                        name: imageUrl.substring(imageUrl.lastIndexOf('/') + 1, imageUrl.indexOf('.', imageUrl.lastIndexOf('/'))),
                        image: htmlImage
                    });
                }
                loadJsonPromise = new Promise(function (resolve, reject) {
                    if (jsonUrl === undefined) {
                        resolve();
                    }
                    else {
                        jsonReq.onreadystatechange = function () {
                            if (jsonReq.readyState === 4) {
                                if (jsonReq.status === 200) {
                                    texturesToCache.splice(0, texturesToCache.length);
                                    var imageCanvas = new OffscreenCanvas(htmlImage.width, htmlImage.height);
                                    var ctx = imageCanvas.getContext("2d");
                                    ctx.drawImage(htmlImage, 0, 0);
                                    var json = JSON.parse(jsonReq.responseText);
                                    for (var i = 0; i < json.frames.length; i++) {
                                        var rect = json.frames[i].frame;
                                        var imgdata = ctx.getImageData(rect.x, rect.y, rect.w, rect.h);
                                        var frameCanvas = document.createElement('canvas');
                                        frameCanvas.id = json.frames[i].filename;
                                        var frameContext = frameCanvas.getContext("2d");
                                        frameContext.putImageData(imgdata, 0, 0);
                                        var data = frameCanvas.toDataURL();
                                        var frame = new Image();
                                        frame.src = data;
                                        var texture = {
                                            name: json.frames[i].filename.substring(0, json.frames[i].filename.indexOf('.')),
                                            image: frame
                                        };
                                        texturesToCache.push(texture);
                                        frameCanvas = null;
                                    }
                                    imageCanvas = null;
                                    resolve();
                                }
                                else {
                                    reject();
                                }
                            }
                        };
                    }
                });
                return [2, new Promise(function (resolve, reject) {
                        var both = Promise.all([loadImagePromise, loadJsonPromise]);
                        both.then(function () {
                            for (var i = 0; i < texturesToCache.length; i++) {
                                TextureCache_1.TextureCache.getInstance().addToCache(texturesToCache[i]);
                            }
                            resolve();
                        });
                        both.catch(function () {
                            reject();
                        });
                    })];
            });
        });
    };
    return TextureLoader;
}());
exports.TextureLoader = TextureLoader;
//# sourceMappingURL=TextureLoader.js.map