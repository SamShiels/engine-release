export declare class TextureLoader {
    static load(imageUrl: string, jsonUrl?: string): Promise<void>;
}
