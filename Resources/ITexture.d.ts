export interface ITexture {
    name: string;
    image: HTMLImageElement;
}
