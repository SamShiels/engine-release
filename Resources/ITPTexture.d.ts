import { IRect } from '../Utils/IRect';
export interface ITPTexture {
    frames: ITPTextureFrame[];
}
export interface ITPTextureFrame {
    filename: string;
    frame: IRect;
    rotated: boolean;
    trimmed: boolean;
    spriteSourceSize: IRect;
    sourceSize: {
        w: number;
        h: number;
    };
}
