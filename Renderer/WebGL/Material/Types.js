"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UniformDataType;
(function (UniformDataType) {
    UniformDataType["FLOAT"] = "FLOAT";
    UniformDataType["INT"] = "INT";
    UniformDataType["VEC2"] = "VEC2";
    UniformDataType["VEC3"] = "VEC3";
    UniformDataType["VEC4"] = "VEC4";
    UniformDataType["MAT2"] = "MAT2";
    UniformDataType["MAT3"] = "MAT3";
    UniformDataType["MAT4"] = "MAT4";
})(UniformDataType = exports.UniformDataType || (exports.UniformDataType = {}));
exports.UniformDataMap = {
    'FLOAT': function (gl, location, value) { gl.uniform1f(location, value); },
    'INT': function (gl, location, value) { gl.uniform1i(location, value); },
    'VEC2': function (gl, location, value) { gl.uniform2fv(location, value); },
    'VEC3': function (gl, location, value) { gl.uniform3fv(location, value); },
    'VEC4': function (gl, location, value) { gl.uniform4fv(location, value); },
    'MAT2': function (gl, location, value) { gl.uniformMatrix2fv(location, false, value); },
    'MAT3': function (gl, location, value) { gl.uniformMatrix3fv(location, false, value); },
    'MAT4': function (gl, location, value) { gl.uniformMatrix4fv(location, false, value); },
};
//# sourceMappingURL=Types.js.map