import { ITexture } from "../Texture/ITexture";
import { IUniforms } from "./Types";
export interface IAttributeLocations {
    [key: string]: number;
}
export interface IUniformLocations {
    [key: string]: WebGLUniformLocation;
}
export interface IMaterial {
    shaderProgram: WebGLProgram;
    attributeLocations: IAttributeLocations;
    uniformLocations: IUniformLocations;
    uniformData: IUniforms;
    texture: ITexture;
    image?: HTMLImageElement;
}
