"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TextureCache_1 = require("../../../Resources/TextureCache");
var WebGLUtils_1 = require("../Renderer/WebGLUtils");
var Types_1 = require("./Types");
var MaterialManager = (function () {
    function MaterialManager(gl) {
        this._gl = gl;
        this._materials = new Map();
        var vs = "\n\t\tattribute vec4 a_position;\n\t\tattribute vec2 a_texcoord;\n\t\tattribute vec4 a_color;\n\n\t\tuniform mat4 u_projMatrix;\n\n\t\tvarying vec2 v_texcoord;\n\t\tvarying vec4 v_color;\n\n\t\tvoid main() {\n\t\t\tvec4 position = u_projMatrix * a_position;\n\t\t\tgl_Position = position;\n\t\t\tv_texcoord = a_texcoord;\n\t\t\tv_color = a_color;\n\t\t}";
        var fs = "\n\t\tprecision mediump float;\n\t\t\n\t\tvarying vec2 v_texcoord;\n\t\tvarying lowp vec4 v_color;\n\n\t\tvoid main() {\n\t\t\tvec4 color = v_color;\n\t\t\tgl_FragColor = color;\n\t\t}";
        this.createMaterial('lit', vs, fs, {});
    }
    Object.defineProperty(MaterialManager.prototype, "materials", {
        get: function () {
            return this._materials;
        },
        enumerable: true,
        configurable: true
    });
    MaterialManager.prototype.createMaterial = function (id, vertexShader, fragmentShader, uniforms, texture) {
        if (this._materials.has(id)) {
            console.error('Duplicate material ID!');
            return;
        }
        var program = WebGLUtils_1.WebGLUtils.createProgram(this._gl, vertexShader, fragmentShader);
        var attributeLocations = {
            a_position: this._gl.getAttribLocation(program, 'a_position'),
            a_texcoord: this._gl.getAttribLocation(program, 'a_texcoord'),
            a_color: this._gl.getAttribLocation(program, 'a_color'),
        };
        var uniformLocations = {
            u_projMatrix: this._gl.getUniformLocation(program, 'u_projMatrix'),
        };
        var names = Object.keys(uniforms);
        for (var i = 0; i < names.length; i++) {
            var name_1 = names[i];
            uniformLocations[names[i]] = this._gl.getUniformLocation(program, name_1);
        }
        var image;
        var glTexture;
        if (texture) {
            var gl = this._gl;
            image = TextureCache_1.TextureCache.getInstance().get(texture.imageId).image;
            glTexture = {
                glTexture: gl.createTexture(),
                filter: texture.filter
            };
        }
        this._materials.set(id, {
            shaderProgram: program,
            attributeLocations: attributeLocations,
            uniformLocations: uniformLocations,
            uniformData: uniforms,
            image: image,
            texture: glTexture
        });
    };
    MaterialManager.prototype.updateUniforms = function (materialId, uniforms) {
        var material = this._materials.get(materialId);
        material.uniformData = uniforms;
    };
    MaterialManager.prototype.applyUniforms = function (materialId, uniforms) {
        var material = this._materials.get(materialId);
        var uniformNames = Object.keys(uniforms);
        for (var i = 0; i < uniformNames.length; i++) {
            var uniformName = uniformNames[i];
            var uniform = uniforms[uniformName];
            Types_1.UniformDataMap[uniform.dataType](this._gl, material.uniformLocations[uniformName], uniform.value);
        }
    };
    return MaterialManager;
}());
exports.MaterialManager = MaterialManager;
//# sourceMappingURL=MaterialManager.js.map