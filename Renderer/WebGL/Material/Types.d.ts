import { mat2, mat3, mat4, vec2, vec3, vec4 } from "gl-matrix";
export declare enum UniformDataType {
    FLOAT = "FLOAT",
    INT = "INT",
    VEC2 = "VEC2",
    VEC3 = "VEC3",
    VEC4 = "VEC4",
    MAT2 = "MAT2",
    MAT3 = "MAT3",
    MAT4 = "MAT4"
}
export declare const UniformDataMap: {
    FLOAT: (gl: WebGLRenderingContext, location: WebGLUniformLocation, value: number) => void;
    INT: (gl: WebGLRenderingContext, location: WebGLUniformLocation, value: number) => void;
    VEC2: (gl: WebGLRenderingContext, location: WebGLUniformLocation, value: vec2) => void;
    VEC3: (gl: WebGLRenderingContext, location: WebGLUniformLocation, value: vec3) => void;
    VEC4: (gl: WebGLRenderingContext, location: WebGLUniformLocation, value: vec4) => void;
    MAT2: (gl: WebGLRenderingContext, location: WebGLUniformLocation, value: mat2) => void;
    MAT3: (gl: WebGLRenderingContext, location: WebGLUniformLocation, value: mat3) => void;
    MAT4: (gl: WebGLRenderingContext, location: WebGLUniformLocation, value: mat4) => void;
};
export interface IUniform {
    dataType: UniformDataType;
    value: any;
}
export interface IUniforms {
    [key: string]: IUniform;
}
