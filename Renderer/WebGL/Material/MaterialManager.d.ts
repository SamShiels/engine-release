import { ITexture as IWorldTexture } from "../../../World/ITexture";
import { IMaterial } from "./IMaterial";
import { IUniforms } from "./Types";
export declare class MaterialManager {
    private _gl;
    private _materials;
    get materials(): Map<string, IMaterial>;
    constructor(gl: WebGLRenderingContext);
    createMaterial(id: string, vertexShader: string, fragmentShader: string, uniforms: IUniforms, texture?: IWorldTexture): void;
    updateUniforms(materialId: string, uniforms: IUniforms): void;
    applyUniforms(materialId: string, uniforms: IUniforms): void;
}
