import { IMaterial } from "../../../World/IMaterial";
export declare class MaterialCache {
    private _gl;
    private _materials;
    private _programs;
    constructor(gl: WebGLRenderingContext, materials: IMaterial[]);
    getMaterial(index: number): IMaterial;
}
