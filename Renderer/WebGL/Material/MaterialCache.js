"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WebGLUtils_1 = require("../Renderer/WebGLUtils");
var MaterialCache = (function () {
    function MaterialCache(gl, materials) {
        this._materials = [];
        this._programs = [];
        this._gl = gl;
        for (var i = 0; i < materials.length; i++) {
            var mat = materials[i];
            this._programs.push(WebGLUtils_1.WebGLUtils.createProgram(gl, mat.vertexShader, mat.fragmentShader));
        }
    }
    MaterialCache.prototype.getMaterial = function (index) {
        return this._materials[index];
    };
    return MaterialCache;
}());
exports.MaterialCache = MaterialCache;
//# sourceMappingURL=MaterialCache.js.map