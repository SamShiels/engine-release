export interface IMaterialData {
    positionAttributeLocation: number;
    texCoordAttributeLocation: number;
    colorAttributeLocation: number;
    projectionMatrixUniformLocation: WebGLUniformLocation;
    imageUniformLocation: WebGLUniformLocation;
    image?: HTMLImageElement;
}
