import { TextureFilter } from "./TextureFilter";
export interface ITexture {
    glTexture: WebGLTexture;
    filter?: TextureFilter;
}
