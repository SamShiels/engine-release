export declare enum TextureFilter {
    LINEAR = "LINEAR",
    NEAREST = "NEAREST"
}
