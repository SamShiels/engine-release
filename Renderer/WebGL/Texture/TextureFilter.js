"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TextureFilter;
(function (TextureFilter) {
    TextureFilter["LINEAR"] = "LINEAR";
    TextureFilter["NEAREST"] = "NEAREST";
})(TextureFilter = exports.TextureFilter || (exports.TextureFilter = {}));
;
//# sourceMappingURL=TextureFilter.js.map