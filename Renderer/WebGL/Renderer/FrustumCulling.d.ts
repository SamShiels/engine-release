import { vec2 } from "gl-matrix";
export declare class FrustumCulling {
    static testPoint(viewportWidth: number, viewportHeight: number, point: vec2): boolean;
}
