"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var gl_matrix_1 = require("gl-matrix");
var WebGLUtils_1 = require("./WebGLUtils");
var MaterialManager_1 = require("../Material/MaterialManager");
var FrustumCulling_1 = require("./FrustumCulling");
var TextureFilter_1 = require("../Texture/TextureFilter");
var WebGLRenderer = (function () {
    function WebGLRenderer(gl, backgroundColor) {
        this._canvasWidth = 0;
        this._canvasHeight = 0;
        this._backgroundColor = { r: 0, g: 0, b: 0 };
        this._meshes = [];
        this._gl = gl;
        if (backgroundColor) {
            this._backgroundColor = backgroundColor;
        }
        this._canvasWidth = gl.canvas.width;
        this._canvasHeight = gl.canvas.height;
        this._materialManager = new MaterialManager_1.MaterialManager(gl);
        if (!this._gl) {
            console.warn('Crikey mate... no WebGL!');
            return;
        }
        this._vertexBuffer = this._gl.createBuffer();
        this._indexBuffer = this._gl.createBuffer();
        this._textureBuffer = this._gl.createBuffer();
        this._colorBuffer = this._gl.createBuffer();
        var viewProjMatrix = new Float32Array([
            2 / this._canvasWidth, 0.0, 0.0, 0.0,
            0.0, -2 / this._canvasHeight, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            -1.0, 1.0, 0.0, 1.0,
        ]);
        this._viewProjectionMatrix = viewProjMatrix;
    }
    Object.defineProperty(WebGLRenderer.prototype, "canvasWidth", {
        get: function () {
            return this._canvasWidth;
        },
        set: function (value) {
            this._canvasWidth = value;
            var viewProjMatrix = new Float32Array([
                2 / this._canvasWidth, 0.0, 0.0, 0.0,
                0.0, -2 / this._canvasHeight, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                -1.0, 1.0, 0.0, 1.0,
            ]);
            this._viewProjectionMatrix = viewProjMatrix;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebGLRenderer.prototype, "canvasHeight", {
        get: function () {
            return this._canvasHeight;
        },
        set: function (value) {
            this._canvasHeight = value;
            var viewProjMatrix = new Float32Array([
                2 / this._canvasWidth, 0.0, 0.0, 0.0,
                0.0, -2 / this._canvasHeight, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                -1.0, 1.0, 0.0, 1.0,
            ]);
            this._viewProjectionMatrix = viewProjMatrix;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebGLRenderer.prototype, "backgroundColor", {
        get: function () {
            return this._backgroundColor;
        },
        set: function (value) {
            this._backgroundColor = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebGLRenderer.prototype, "materialManager", {
        get: function () {
            return this._materialManager;
        },
        enumerable: true,
        configurable: true
    });
    WebGLRenderer.prototype.renderLoop = function () {
        this._render();
    };
    WebGLRenderer.prototype.fixedLoop = function () {
    };
    WebGLRenderer.prototype._render = function () {
        var _this = this;
        var gl = this._gl;
        gl.viewport(0, 0, this._canvasWidth, this._canvasHeight);
        gl.clearColor(this._backgroundColor.r, this._backgroundColor.g, this._backgroundColor.b, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        gl.depthFunc(gl.LEQUAL);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
        gl.enable(gl.BLEND);
        gl.blendEquation(gl.FUNC_ADD);
        gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
        gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
        if (this._meshes.length === 0) {
            return;
        }
        var renderGroups = this._materialManager.materials;
        renderGroups.forEach(function (group, id) {
            var indices = [];
            var vertices = [];
            var texcoords = [];
            var colors = [];
            for (var i = 0; i < _this._meshes.length; i++) {
                var mesh = _this._meshes[i];
                if (mesh.enabledInHierarchy === false || (mesh.material && mesh.material.id !== id)) {
                    continue;
                }
                var xBoundMin = 0;
                var xBoundMax = 0;
                var yBoundMin = 0;
                var yBoundMax = 0;
                var meshWorldVerts = [];
                for (var j = 0; j < mesh.vertices.length; j += 2) {
                    var x = mesh.vertices[j];
                    var y = mesh.vertices[j + 1];
                    var z = mesh.zIndex;
                    var v = gl_matrix_1.vec2.fromValues(x, y);
                    gl_matrix_1.vec2.transformMat2d(v, v, mesh.getLocalToWorldMatrix());
                    var x2 = v[0];
                    var y2 = v[1];
                    if (j === 0) {
                        xBoundMin = x2;
                        xBoundMax = x2;
                        yBoundMin = y2;
                        yBoundMax = y2;
                    }
                    else {
                        if (x2 < xBoundMin) {
                            xBoundMin = x2;
                        }
                        else if (x2 > xBoundMax) {
                            xBoundMax = x2;
                        }
                        if (y2 < yBoundMin) {
                            yBoundMin = y2;
                        }
                        else if (y2 > yBoundMax) {
                            yBoundMax = y2;
                        }
                    }
                    meshWorldVerts.push(x2);
                    meshWorldVerts.push(y2);
                    meshWorldVerts.push(z);
                }
                var meshMithinView = false;
                for (var j = 0; j < 4; j++) {
                    var point = [0, 0];
                    if (j === 0) {
                        point = [xBoundMin, yBoundMin];
                    }
                    else if (j === 1) {
                        point = [xBoundMax, yBoundMin];
                    }
                    else if (j === 2) {
                        point = [xBoundMax, yBoundMax];
                    }
                    else if (j === 3) {
                        point = [xBoundMin, yBoundMax];
                    }
                    var withinView = FrustumCulling_1.FrustumCulling.testPoint(_this._canvasWidth, _this._canvasHeight, point);
                    if (withinView) {
                        meshMithinView = true;
                    }
                }
                if (!meshMithinView) {
                    continue;
                }
                var globalIndices = mesh.indices.map(function (index) {
                    return (vertices.length / 3) + index;
                });
                indices = indices.concat(globalIndices);
                vertices = vertices.concat(meshWorldVerts);
                texcoords = texcoords.concat(mesh.texcoords);
                colors = colors.concat(mesh.colors);
            }
            gl.useProgram(group.shaderProgram);
            gl.bindBuffer(gl.ARRAY_BUFFER, _this._vertexBuffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.DYNAMIC_DRAW);
            WebGLUtils_1.WebGLUtils.attribPointer(gl, group.attributeLocations.a_position, 3, gl.FLOAT, false, 0, 0);
            gl.bindBuffer(gl.ARRAY_BUFFER, _this._textureBuffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texcoords), gl.DYNAMIC_DRAW);
            WebGLUtils_1.WebGLUtils.attribPointer(gl, group.attributeLocations.a_texcoord, 2, gl.FLOAT, false, 0, 0);
            if (group.attributeLocations.a_color > -1) {
                gl.bindBuffer(gl.ARRAY_BUFFER, _this._colorBuffer);
                gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.DYNAMIC_DRAW);
                WebGLUtils_1.WebGLUtils.attribPointer(gl, group.attributeLocations.a_color, 4, gl.FLOAT, false, 0, 0);
            }
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, _this._indexBuffer);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.DYNAMIC_DRAW);
            gl.enable(gl.CULL_FACE);
            gl.enable(gl.DEPTH_TEST);
            if (id !== 'lit') {
                if (group.image) {
                    var texture = group.texture;
                    var image = group.image;
                    gl.bindTexture(gl.TEXTURE_2D, texture.glTexture);
                    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
                    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
                    if (texture.filter) {
                        if (texture.filter === TextureFilter_1.TextureFilter.LINEAR) {
                            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
                        }
                        else if (texture.filter === TextureFilter_1.TextureFilter.NEAREST) {
                            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
                        }
                    }
                    else {
                        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
                    }
                    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
                }
                _this._materialManager.applyUniforms(id, group.uniformData);
            }
            gl.uniformMatrix4fv(group.uniformLocations.u_projMatrix, false, _this._viewProjectionMatrix);
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, _this._indexBuffer);
            gl.drawElements(gl.TRIANGLES, indices.length, gl.UNSIGNED_SHORT, 0);
        });
    };
    WebGLRenderer.prototype.addObject = function (object) {
        this._meshes.push(object);
        this._meshes.sort();
    };
    WebGLRenderer.prototype.removeObject = function (object) {
        this._meshes.splice(this._meshes.indexOf(object), 1);
    };
    return WebGLRenderer;
}());
exports.WebGLRenderer = WebGLRenderer;
//# sourceMappingURL=WebGLRenderer.js.map