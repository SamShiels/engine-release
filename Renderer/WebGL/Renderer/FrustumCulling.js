"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FrustumCulling = (function () {
    function FrustumCulling() {
    }
    FrustumCulling.testPoint = function (viewportWidth, viewportHeight, point) {
        var x = point[0];
        var y = point[1];
        return x >= 0 && x <= viewportWidth && y >= 0 && y <= viewportHeight;
    };
    return FrustumCulling;
}());
exports.FrustumCulling = FrustumCulling;
//# sourceMappingURL=FrustumCulling.js.map