export declare class WebGLUtils {
    static isPowerOf2(value: any): boolean;
    static createProgram(gl: WebGLRenderingContext, vertexShaderSource: string, fragmentShaderSource: string): WebGLProgram;
    private static _createShader;
    static attribPointer(gl: WebGLRenderingContext, location: number, dimensions: number, type: number, normalized: boolean, stride: number, offset: number): void;
}
