"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WebGLUtils = (function () {
    function WebGLUtils() {
    }
    WebGLUtils.isPowerOf2 = function (value) {
        return (value & (value - 1)) === 0;
    };
    WebGLUtils.createProgram = function (gl, vertexShaderSource, fragmentShaderSource) {
        var vs = this._createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
        var fs = this._createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
        var program = gl.createProgram();
        gl.attachShader(program, vs);
        gl.attachShader(program, fs);
        gl.linkProgram(program);
        var success = gl.getProgramParameter(program, gl.LINK_STATUS);
        if (success) {
            return program;
        }
        console.log(gl.getProgramInfoLog(program));
        gl.deleteProgram(program);
    };
    WebGLUtils._createShader = function (gl, type, source) {
        var shader = gl.createShader(type);
        gl.shaderSource(shader, source);
        gl.compileShader(shader);
        var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
        if (success) {
            return shader;
        }
        console.log(gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
    };
    WebGLUtils.attribPointer = function (gl, location, dimensions, type, normalized, stride, offset) {
        gl.enableVertexAttribArray(location);
        gl.vertexAttribPointer(location, dimensions, type, normalized, stride, offset);
    };
    return WebGLUtils;
}());
exports.WebGLUtils = WebGLUtils;
//# sourceMappingURL=WebGLUtils.js.map