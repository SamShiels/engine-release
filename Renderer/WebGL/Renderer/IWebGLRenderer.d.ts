import { IColor } from "../../../Utils/IColor";
import { IMesh } from "../../../World/IMesh";
import { MaterialManager } from "../Material/MaterialManager";
export interface IWebGLRenderer {
    canvasWidth: number;
    canvasHeight: number;
    backgroundColor: IColor;
    materialManager: MaterialManager;
    addObject(control: IMesh): void;
    removeObject(control: IMesh): void;
}
