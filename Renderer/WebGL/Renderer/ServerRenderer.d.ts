import { ISystem } from '../../../ISystem';
import { IColor } from '../../../Utils/IColor';
import { IMesh } from '../../../World/IMesh';
import { MaterialManager } from '../Material/MaterialManager';
import { IWebGLRenderer } from './IWebGLRenderer';
export declare class ServerRenderer implements IWebGLRenderer, ISystem {
    get canvasWidth(): number;
    get canvasHeight(): number;
    get materialManager(): MaterialManager;
    get backgroundColor(): IColor;
    renderLoop(): void;
    fixedLoop(): void;
    addObject(object: IMesh): void;
    removeObject(object: IMesh): void;
}
