"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ServerRenderer = (function () {
    function ServerRenderer() {
    }
    Object.defineProperty(ServerRenderer.prototype, "canvasWidth", {
        get: function () {
            return 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ServerRenderer.prototype, "canvasHeight", {
        get: function () {
            return 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ServerRenderer.prototype, "materialManager", {
        get: function () {
            return null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ServerRenderer.prototype, "backgroundColor", {
        get: function () {
            return { r: 0, g: 0, b: 0 };
        },
        enumerable: true,
        configurable: true
    });
    ServerRenderer.prototype.renderLoop = function () {
    };
    ServerRenderer.prototype.fixedLoop = function () {
    };
    ServerRenderer.prototype.addObject = function (object) {
    };
    ServerRenderer.prototype.removeObject = function (object) {
    };
    return ServerRenderer;
}());
exports.ServerRenderer = ServerRenderer;
//# sourceMappingURL=ServerRenderer.js.map