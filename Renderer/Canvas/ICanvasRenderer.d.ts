import { IGUI } from "../../GUI/IGUI";
export interface ICanvasRenderer {
    canvasWidth: number;
    canvasHeight: number;
    addControl(control: IGUI): void;
    removeControl(control: IGUI): void;
}
