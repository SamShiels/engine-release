"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CanvasRenderer = (function () {
    function CanvasRenderer(context, imageSmoothing) {
        this._canvasWidth = 0;
        this._canvasHeight = 0;
        this._controls = [];
        this._canvasWidth = context.canvas.width;
        this._canvasHeight = context.canvas.height;
        if (context === null) {
            console.warn('Context is null');
            return;
        }
        context.imageSmoothingEnabled = imageSmoothing !== undefined ? imageSmoothing : true;
        this._context = context;
    }
    Object.defineProperty(CanvasRenderer.prototype, "canvasWidth", {
        get: function () {
            return this._canvasWidth;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CanvasRenderer.prototype, "canvasHeight", {
        get: function () {
            return this._canvasHeight;
        },
        enumerable: true,
        configurable: true
    });
    CanvasRenderer.prototype.renderLoop = function () {
        this._render();
    };
    CanvasRenderer.prototype.fixedLoop = function () {
    };
    CanvasRenderer.prototype._render = function () {
        this._context.clearRect(0, 0, this._canvasWidth, this._canvasHeight);
        this._controls.sort(function (a, b) { return a.zIndex - b.zIndex; });
        for (var i = 0; i < this._controls.length; i++) {
            this._controls[i].render(this._context);
        }
    };
    CanvasRenderer.prototype.addControl = function (control) {
        this._controls.push(control);
    };
    CanvasRenderer.prototype.removeControl = function (control) {
        this._controls.splice(this._controls.indexOf(control), 1);
    };
    return CanvasRenderer;
}());
exports.CanvasRenderer = CanvasRenderer;
//# sourceMappingURL=CanvasRenderer.js.map