import { ICanvasRenderer } from "./ICanvasRenderer";
import { IGUI } from "../../GUI/IGUI";
import { ISystem } from "../../ISystem";
export declare class CanvasRenderer implements ICanvasRenderer, ISystem {
    private _context;
    private _canvasWidth;
    get canvasWidth(): number;
    private _canvasHeight;
    get canvasHeight(): number;
    private _controls;
    constructor(context: CanvasRenderingContext2D, imageSmoothing?: boolean);
    renderLoop(): void;
    fixedLoop(): void;
    private _render;
    addControl(control: IGUI): void;
    removeControl(control: IGUI): void;
}
