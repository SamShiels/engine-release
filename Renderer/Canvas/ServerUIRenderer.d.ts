import { ICanvasRenderer } from './ICanvasRenderer';
import { IGUI } from '../../GUI/IGUI';
import { ISystem } from '../../ISystem';
export declare class ServerUIRenderer implements ICanvasRenderer, ISystem {
    private _context;
    get canvasWidth(): number;
    get canvasHeight(): number;
    renderLoop(): void;
    fixedLoop(): void;
    addControl(control: IGUI): void;
    removeControl(control: IGUI): void;
}
