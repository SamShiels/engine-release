"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ServerUIRenderer = (function () {
    function ServerUIRenderer() {
    }
    Object.defineProperty(ServerUIRenderer.prototype, "canvasWidth", {
        get: function () {
            return 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ServerUIRenderer.prototype, "canvasHeight", {
        get: function () {
            return 0;
        },
        enumerable: true,
        configurable: true
    });
    ServerUIRenderer.prototype.renderLoop = function () {
    };
    ServerUIRenderer.prototype.fixedLoop = function () {
    };
    ServerUIRenderer.prototype.addControl = function (control) {
    };
    ServerUIRenderer.prototype.removeControl = function (control) {
    };
    return ServerUIRenderer;
}());
exports.ServerUIRenderer = ServerUIRenderer;
//# sourceMappingURL=ServerUIRenderer.js.map