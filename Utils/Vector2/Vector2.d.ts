import { IVector2 } from "./IVector2";
export declare class Vector2 {
    private constructor();
    static zero(): IVector2;
    static distance(a: IVector2, b: IVector2): number;
    static sqaureDistance(a: IVector2, b: IVector2): number;
    static add(a: IVector2, b: IVector2): IVector2;
    static subtract(a: IVector2, b: IVector2): IVector2;
    static magnitude(v: IVector2): number;
    static squareMagnitude(v: IVector2): number;
    static normalize(v: IVector2): IVector2;
    static interpolate(a: IVector2, b: IVector2, progress?: number): IVector2;
}
