"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Vector2 = (function () {
    function Vector2() {
    }
    Vector2.zero = function () {
        return {
            x: 0,
            y: 0
        };
    };
    Vector2.distance = function (a, b) {
        return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
    };
    Vector2.sqaureDistance = function (a, b) {
        return Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2);
    };
    Vector2.add = function (a, b) {
        return {
            x: a.x + b.x,
            y: a.y + b.y
        };
    };
    Vector2.subtract = function (a, b) {
        return {
            x: a.x - b.x,
            y: a.y - b.y
        };
    };
    Vector2.magnitude = function (v) {
        return Math.sqrt(Math.pow(v.x, 2) + Math.pow(v.y, 2));
    };
    Vector2.squareMagnitude = function (v) {
        return Math.pow(v.x, 2) + Math.pow(v.y, 2);
    };
    Vector2.normalize = function (v) {
        var magnitude = this.magnitude(v);
        if (magnitude === 0) {
            return;
        }
        return {
            x: v.x / magnitude,
            y: v.y / magnitude
        };
    };
    Vector2.interpolate = function (a, b, progress) {
        if (progress === void 0) { progress = 0.5; }
        return {
            x: a.x + (b.x - a.x) * progress,
            y: a.y + (b.y - a.y) * progress
        };
    };
    return Vector2;
}());
exports.Vector2 = Vector2;
//# sourceMappingURL=Vector2.js.map