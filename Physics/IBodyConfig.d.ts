export interface IBodyConfig {
    mass?: number;
    drag?: number;
    angularDrag?: number;
    inertia?: number;
}
