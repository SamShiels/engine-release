import { Vector2 } from "../Utils/Vector2/Vector2";
export interface IRigidbody {
    mass: number;
    drag: number;
    angularDrag: number;
    inertia: number;
    velocity: Vector2;
    angularVelocity: number;
    isTrigger: boolean;
    freezeRotation: boolean;
    setPosition(position: Vector2): void;
    setRotation(rotation: number): void;
    addForce(force: Vector2): void;
    addTorque(torque: number): void;
    setVelocity(velocity: Vector2): void;
    setAngularVelocity(angularVelocity: number): void;
    onCollisionEnter(callback: Function): void;
    onCollisionExit(callback: Function): void;
}
