import { Shape } from "./Shape";
import { IVector2 } from "../Utils/Vector2/IVector2";
export interface ICollider {
    shape: Shape;
    offset?: IVector2;
}
