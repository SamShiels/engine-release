import { IPhysics, ColliderUnion } from "./IPhysics";
import { IGameObject } from "../World/IGameObject";
import { IBodyConfig } from "./IBodyConfig";
import { IVector2 } from "../Utils/Vector2/IVector2";
import { ISystem } from "../ISystem";
export declare class Physics implements IPhysics, ISystem {
    private _world;
    private _bodies;
    private _lastTime;
    get gravity(): IVector2;
    set gravity(value: IVector2);
    constructor();
    private _syncBody;
    renderLoop(time: number): void;
    fixedLoop(): void;
    addBody(gameObject: IGameObject, config: IBodyConfig, collider: ColliderUnion): void;
    removeBody(gameObject: IGameObject): void;
}
