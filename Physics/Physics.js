"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Rigidbody_1 = require("./Rigidbody");
var Shape_1 = require("./Shape");
var p2_1 = require("p2");
var Physics = (function () {
    function Physics() {
        this._bodies = new Map();
        this._world = new p2_1.World({
            gravity: [0, 90.82]
        });
    }
    Object.defineProperty(Physics.prototype, "gravity", {
        get: function () {
            return { x: this._world.gravity[0], y: this._world.gravity[1] };
        },
        set: function (value) {
            this._world.gravity[0] = value.x;
            this._world.gravity[1] = value.y;
        },
        enumerable: true,
        configurable: true
    });
    Physics.prototype._syncBody = function (body) {
        body.gameObject.rigidbodySetPosition({
            x: body.body.position[0] - (body.collider.offset ? body.collider.offset.x : 0),
            y: body.body.position[1] - (body.collider.offset ? body.collider.offset.y : 0)
        });
        body.gameObject.rigidbodySetRotation(body.body.angle * 180 / Math.PI);
    };
    Physics.prototype.renderLoop = function (time) {
        this._bodies.forEach(this._syncBody);
        var dt = this._lastTime ? (time - this._lastTime) / 1000 : 0;
        this._world.step(1 / 60, dt, 10);
        this._lastTime = time;
    };
    Physics.prototype.fixedLoop = function () {
    };
    Physics.prototype.addBody = function (gameObject, config, collider) {
        var body = new p2_1.Body({
            mass: config.mass,
            position: [gameObject.position.x, gameObject.position.y],
            angle: gameObject.rotation,
        });
        body.damping = config.drag ? config.drag : 0.1;
        body.angularDamping = config.angularDrag ? config.angularDrag : 0.1;
        if (collider.shape === Shape_1.Shape.Box) {
            var boxCollider = collider;
            var shape = new p2_1.Box({
                width: boxCollider.size.x,
                height: boxCollider.size.y
            });
            body.addShape(shape);
        }
        else if (collider.shape === Shape_1.Shape.Circle) {
            var circleCollider = collider;
            var shape = new p2_1.Circle({
                radius: circleCollider.radius
            });
            body.addShape(shape);
        }
        else if (collider.shape === Shape_1.Shape.Mesh) {
            var meshCollider = collider;
            var vertices = [];
            for (var i = 0; i < meshCollider.vertices.length; i += 2) {
                vertices.push([meshCollider.vertices[i], meshCollider.vertices[i + 1]]);
            }
            body.fromPolygon(vertices, {
                optimalDecomp: false,
                skipSimpleCheck: true,
                removeCollinearPoints: false
            });
        }
        this._world.addBody(body);
        this._bodies.set(gameObject, {
            gameObject: gameObject,
            body: body,
            collider: collider
        });
        var rigidbody = new Rigidbody_1.Rigidbody(body, config);
        gameObject.setRigidbody(rigidbody);
    };
    Physics.prototype.removeBody = function (gameObject) {
        var body = this._bodies.get(gameObject);
        this._world.removeBody(body.body);
        this._bodies.delete(gameObject);
    };
    return Physics;
}());
exports.Physics = Physics;
//# sourceMappingURL=Physics.js.map