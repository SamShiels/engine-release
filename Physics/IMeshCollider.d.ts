import { ICollider } from "./ICollider";
export interface IMeshCollider extends ICollider {
    vertices: number[];
}
