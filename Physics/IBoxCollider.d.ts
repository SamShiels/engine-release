import { ICollider } from "./ICollider";
import { IVector2 } from "../Utils/Vector2/IVector2";
export interface IBoxCollider extends ICollider {
    size: IVector2;
}
