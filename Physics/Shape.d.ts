export declare enum Shape {
    Box = 0,
    Circle = 1,
    Mesh = 2
}
