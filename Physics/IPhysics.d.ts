import { IGameObject } from "../World/IGameObject";
import { IBodyConfig } from "./IBodyConfig";
import { IBoxCollider } from "./IBoxCollider";
import { ICircleCollider } from "./ICircleCollider";
import { IMeshCollider } from "./IMeshCollider";
import { IVector2 } from "../Utils/Vector2/IVector2";
export declare type ColliderUnion = IBoxCollider | ICircleCollider | IMeshCollider;
export interface IPhysics {
    gravity: IVector2;
    addBody(gameObject: IGameObject, config: IBodyConfig, collider: ColliderUnion): void;
    removeBody(gameObject: IGameObject): void;
}
