"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Shape;
(function (Shape) {
    Shape[Shape["Box"] = 0] = "Box";
    Shape[Shape["Circle"] = 1] = "Circle";
    Shape[Shape["Mesh"] = 2] = "Mesh";
})(Shape = exports.Shape || (exports.Shape = {}));
//# sourceMappingURL=Shape.js.map