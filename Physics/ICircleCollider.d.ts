import { ICollider } from "./ICollider";
export interface ICircleCollider extends ICollider {
    radius: number;
}
