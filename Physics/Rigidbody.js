"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Rigidbody = (function () {
    function Rigidbody(body, config) {
        this._mass = 1;
        this._drag = 0;
        this._angularDrag = 0;
        this._inertia = 0;
        this._freezeRotation = false;
        this._isTrigger = false;
        this._body = body;
        this._mass = config.mass ? config.mass : 1;
        this._drag = config.drag ? config.drag : 0.1;
        this._angularDrag = config.angularDrag ? config.angularDrag : 0.1;
        this._inertia = config.inertia ? config.inertia : 0;
    }
    Object.defineProperty(Rigidbody.prototype, "mass", {
        get: function () {
            return this._mass;
        },
        set: function (value) {
            this._body.mass = value;
            this._mass = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rigidbody.prototype, "drag", {
        get: function () {
            return this._drag;
        },
        set: function (value) {
            this._body.damping = value;
            this._drag = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rigidbody.prototype, "angularDrag", {
        get: function () {
            return this._angularDrag;
        },
        set: function (value) {
            this._body.angularDamping = value;
            this._angularDrag = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rigidbody.prototype, "inertia", {
        get: function () {
            return this._inertia;
        },
        set: function (value) {
            this._body.inertia = value;
            this._inertia = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rigidbody.prototype, "freezeRotation", {
        get: function () {
            return this._freezeRotation;
        },
        set: function (value) {
            this._body.fixedRotation = value;
            this._freezeRotation = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rigidbody.prototype, "isTrigger", {
        get: function () {
            return this._isTrigger;
        },
        set: function (value) {
            this._isTrigger = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rigidbody.prototype, "velocity", {
        get: function () {
            return { x: this._body.velocity[0], y: this._body.velocity[1] };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rigidbody.prototype, "angularVelocity", {
        get: function () {
            return this._body.angularVelocity;
        },
        enumerable: true,
        configurable: true
    });
    Rigidbody.prototype.setPosition = function (position) {
        this._body.position[0] = position.x;
        this._body.position[1] = position.y;
    };
    Rigidbody.prototype.setRotation = function (rotation) {
        this._body.angle = rotation * Math.PI / 180;
    };
    Rigidbody.prototype.addForce = function (force) {
        this._body.applyForce([force.x, force.y]);
    };
    Rigidbody.prototype.addTorque = function (torque) {
        this._body.angularVelocity += torque;
    };
    Rigidbody.prototype.setVelocity = function (velocity) {
        this._body.velocity = [velocity.x, velocity.y];
    };
    Rigidbody.prototype.setAngularVelocity = function (angularVelocity) {
        this._body.angularVelocity = angularVelocity;
    };
    Rigidbody.prototype.onCollisionEnter = function (callback) {
        this._body.on('beginContact', callback);
    };
    Rigidbody.prototype.onCollisionExit = function (callback) {
        this._body.on('endContact', callback);
    };
    return Rigidbody;
}());
exports.Rigidbody = Rigidbody;
//# sourceMappingURL=Rigidbody.js.map