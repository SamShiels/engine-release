import { IMaterial } from "./IMaterial";
import { IUniforms } from "../Renderer/WebGL/Material/Types";
import { ITexture } from "./ITexture";
export declare class Material implements IMaterial {
    private _id;
    get id(): string;
    private _vertexShader;
    get vertexShader(): string;
    private _fragmentShader;
    get fragmentShader(): string;
    private _uniforms;
    get uniforms(): IUniforms;
    set uniforms(value: IUniforms);
    private _texture;
    private _manager;
    constructor(id: string, vertexShader: string, fragmentShader: string, uniforms: IUniforms, texture?: ITexture);
}
