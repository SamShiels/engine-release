"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var gl_matrix_1 = require("gl-matrix");
var GameObject = (function () {
    function GameObject() {
        this._children = [];
        this._enabled = true;
        this._enabledInHierarchy = true;
        this._position = { x: 0, y: 0 };
        this._zIndex = 0;
        this._rotation = 0;
        this._width = 0;
        this._height = 0;
        this._scale = { x: 100, y: 100 };
        this._dirty = false;
        this._inverseDirty = false;
        this._children = [];
        this._dirty = false;
        this._inverseDirty = false;
        this._localToWorldMatrix = gl_matrix_1.mat2d.create();
        this._worldToLocalMatrix = gl_matrix_1.mat2d.create();
        gl_matrix_1.mat2d.identity(this._localToWorldMatrix);
        gl_matrix_1.mat2d.identity(this._worldToLocalMatrix);
    }
    Object.defineProperty(GameObject.prototype, "parent", {
        get: function () {
            return this._parent;
        },
        set: function (value) {
            this._parent = value;
            this.setDirty();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "children", {
        get: function () {
            return this._children;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "enabled", {
        get: function () {
            return this._enabled;
        },
        set: function (value) {
            this._enabled = value;
            this._enabledInHierarchy = value;
            this.hierarchyEnabledStateChanged();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "enabledInHierarchy", {
        get: function () {
            return this._enabledInHierarchy;
        },
        set: function (value) {
            this._enabledInHierarchy = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "position", {
        get: function () {
            return this._position;
        },
        set: function (value) {
            this._position = value;
            if (this._rigidbody) {
                this._rigidbody.setPosition(value);
            }
            this.setDirty();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "zIndex", {
        get: function () {
            return this._zIndex;
        },
        set: function (value) {
            this._zIndex = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "rotation", {
        get: function () {
            return this._rotation;
        },
        set: function (value) {
            this._rotation = value;
            if (this._rigidbody) {
                this._rigidbody.setRotation(value);
            }
            this.setDirty();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "width", {
        get: function () {
            return this._width;
        },
        set: function (value) {
            this._width = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "height", {
        get: function () {
            return this._height;
        },
        set: function (value) {
            this._height = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "scale", {
        get: function () {
            return this._scale;
        },
        set: function (value) {
            this._scale = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameObject.prototype, "rigidbody", {
        get: function () {
            return this._rigidbody;
        },
        enumerable: true,
        configurable: true
    });
    GameObject.prototype.setRigidbody = function (body) {
        this._rigidbody = body;
    };
    GameObject.prototype.rigidbodySetPosition = function (position) {
        this._position = position;
    };
    GameObject.prototype.rigidbodySetRotation = function (rotation) {
        this._rotation = rotation;
    };
    GameObject.prototype.setDirty = function () {
        this._dirty = true;
        this._inverseDirty = true;
        for (var i = 0; i < this._children.length; i++) {
            this._children[i].setDirty();
        }
    };
    GameObject.prototype._calculateLocalToWorldMatrix = function () {
        gl_matrix_1.mat2d.identity(this._localToWorldMatrix);
        gl_matrix_1.mat2d.translate(this._localToWorldMatrix, this._localToWorldMatrix, gl_matrix_1.vec2.fromValues(this._position.x, this._position.y));
        gl_matrix_1.mat2d.rotate(this._localToWorldMatrix, this._localToWorldMatrix, this._rotation * Math.PI / 180);
        gl_matrix_1.mat2d.scale(this._localToWorldMatrix, this._localToWorldMatrix, gl_matrix_1.vec2.fromValues(this._scale.x, this._scale.y));
    };
    GameObject.prototype.getLocalToWorldMatrix = function () {
        if (this._dirty === true) {
            if (this._parent === undefined) {
                this._calculateLocalToWorldMatrix();
            }
            else {
                this._calculateLocalToWorldMatrix();
                gl_matrix_1.mat2d.mul(this._localToWorldMatrix, this._parent.getLocalToWorldMatrix(), this._localToWorldMatrix);
            }
        }
        return this._localToWorldMatrix;
    };
    GameObject.prototype.getWorldToLocalMatrix = function () {
        if (this._inverseDirty === true) {
            gl_matrix_1.mat2d.invert(this._worldToLocalMatrix, this.getLocalToWorldMatrix());
        }
        return this._worldToLocalMatrix;
    };
    GameObject.prototype.hierarchyEnabledStateChanged = function () {
        for (var i = 0; i < this._children.length; i++) {
            this._children[i].enabledInHierarchy = this._enabledInHierarchy;
            this._children[i].hierarchyEnabledStateChanged();
        }
    };
    GameObject.prototype.instantiate = function (constructor, src) {
        var obj = Object.assign(new constructor(), src);
        obj.parent = this;
        this._children.push(obj);
        return obj;
    };
    GameObject.prototype.destroy = function () {
        for (var i = this._children.length - 1; i >= 0; i--) {
            this._children[i].destroy();
        }
        if (this._parent !== undefined) {
            this._parent.children.splice(this._parent.children.indexOf(this), 1);
        }
    };
    return GameObject;
}());
exports.GameObject = GameObject;
//# sourceMappingURL=GameObject.js.map