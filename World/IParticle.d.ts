import { IVector2 } from "../Utils/Vector2/IVector2";
import { IMesh } from "./IMesh";
export interface IParticle {
    mesh: IMesh;
    velocity: IVector2;
    spawnTimestamp: number;
}
