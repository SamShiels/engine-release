"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Engine_1 = require("../Engine");
var Material = (function () {
    function Material(id, vertexShader, fragmentShader, uniforms, texture) {
        this._id = id;
        this._vertexShader = vertexShader;
        this._fragmentShader = fragmentShader;
        this._uniforms = uniforms;
        this._texture = texture;
        var renderer = Engine_1.Engine.retrieveSystem('renderer');
        this._manager = renderer.materialManager;
        this._manager.createMaterial(id, vertexShader, fragmentShader, uniforms, texture);
    }
    Object.defineProperty(Material.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Material.prototype, "vertexShader", {
        get: function () {
            return this._vertexShader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Material.prototype, "fragmentShader", {
        get: function () {
            return this._fragmentShader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Material.prototype, "uniforms", {
        get: function () {
            return this._uniforms;
        },
        set: function (value) {
            this._uniforms = value;
            this._manager.updateUniforms(this._id, value);
        },
        enumerable: true,
        configurable: true
    });
    return Material;
}());
exports.Material = Material;
//# sourceMappingURL=Material.js.map