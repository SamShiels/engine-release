import { Constructor, GameObjectProperties } from "./Types";
import { mat2d } from "gl-matrix";
import { IRigidbody } from "../Physics/IRigidbody";
import { IVector2 } from "../Utils/Vector2/IVector2";
export interface IGameObject {
    parent?: IGameObject;
    children: IGameObject[];
    enabled: boolean;
    enabledInHierarchy: boolean;
    position: IVector2;
    zIndex: number;
    rotation: number;
    width: number;
    height: number;
    scale: IVector2;
    rigidbody: IRigidbody | null;
    setRigidbody(body: IRigidbody): void;
    rigidbodySetPosition(position: IVector2): void;
    rigidbodySetRotation(rotation: number): void;
    setDirty(): void;
    getLocalToWorldMatrix(): mat2d;
    hierarchyEnabledStateChanged(): void;
    instantiate<T extends IGameObject>(constructor: Constructor<T>, properties: Omit<GameObjectProperties<T>, 'parent' | 'children'>): T;
    destroy(): void;
}
