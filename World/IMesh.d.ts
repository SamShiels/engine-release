import { IGameObject } from "./IGameObject";
import { IMaterial } from "./IMaterial";
export interface IMesh extends IGameObject {
    material: IMaterial | null;
    vertices: number[];
    indices: number[];
    texcoords: number[];
    colors: number[];
}
