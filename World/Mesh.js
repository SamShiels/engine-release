"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Engine_1 = require("../Engine");
var GameObject_1 = require("./GameObject");
var Mesh = (function (_super) {
    __extends(Mesh, _super);
    function Mesh() {
        var _this = _super.call(this) || this;
        _this._material = null;
        _this._vertices = [
            0, 0,
            1, 0,
            1, 1,
            0, 1
        ];
        _this._indices = [
            0, 2, 1,
            0, 3, 2
        ];
        _this._texcoords = [
            0, 1,
            1, 1,
            1, 0,
            0, 0
        ];
        _this._colors = [
            1, 1, 1, 1,
            1, 1, 1, 1,
            1, 1, 1, 1,
            1, 1, 1, 1
        ];
        _this._renderer = Engine_1.Engine.retrieveSystem('renderer');
        _this._renderer.addObject(_this);
        return _this;
    }
    Object.defineProperty(Mesh.prototype, "material", {
        get: function () {
            return this._material;
        },
        set: function (value) {
            this._material = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Mesh.prototype, "vertices", {
        get: function () {
            return this._vertices;
        },
        set: function (value) {
            this._vertices = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Mesh.prototype, "indices", {
        get: function () {
            return this._indices;
        },
        set: function (value) {
            this._indices = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Mesh.prototype, "texcoords", {
        get: function () {
            return this._texcoords;
        },
        set: function (value) {
            this._texcoords = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Mesh.prototype, "colors", {
        get: function () {
            return this._colors;
        },
        set: function (value) {
            this._colors = value;
        },
        enumerable: true,
        configurable: true
    });
    Mesh.prototype.destroy = function () {
        this._renderer.removeObject(this);
        this._renderer = null;
        this._vertices = null;
        this._indices = null;
        this._texcoords = null;
        this._colors = null;
        _super.prototype.destroy.call(this);
    };
    return Mesh;
}(GameObject_1.GameObject));
exports.Mesh = Mesh;
//# sourceMappingURL=Mesh.js.map