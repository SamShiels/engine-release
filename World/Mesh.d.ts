import { GameObject } from "./GameObject";
import { IMaterial } from "./IMaterial";
import { IMesh } from "./IMesh";
export declare class Mesh extends GameObject implements IMesh {
    private _renderer;
    protected _material: IMaterial | null;
    get material(): IMaterial | null;
    set material(value: IMaterial | null);
    protected _vertices: number[];
    get vertices(): number[];
    set vertices(value: number[]);
    protected _indices: number[];
    get indices(): number[];
    set indices(value: number[]);
    protected _texcoords: number[];
    get texcoords(): number[];
    set texcoords(value: number[]);
    protected _colors: number[];
    get colors(): number[];
    set colors(value: number[]);
    constructor();
    destroy(): void;
}
