import { IGUI } from "../GUI/IGUI";
import { ITexture } from "../Resources/ITexture";
export interface IMask extends IGUI {
    texture: string;
    image: ITexture | undefined;
}
