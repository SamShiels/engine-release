import { IGameObject } from "./IGameObject";
import { GameObjectProperties } from "./Types";
import { Mesh } from "./Mesh";
import { IVector2 } from "../Utils/Vector2/IVector2";
export interface IParticleSystem extends IGameObject {
    prefab: GameObjectProperties<Mesh>;
    spawnRate: number;
    spawnArea: IVector2;
    startVelocityMin: IVector2;
    startVelocityMax: IVector2;
    startSize: IVector2;
    endSize: IVector2;
    lifeSpan: number;
    simulateInWorldSpace: boolean;
    start(): void;
    stop(): void;
    isRunning(): boolean;
}
