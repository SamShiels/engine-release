export declare type Constructor<T> = new (...args: any[]) => T;
export declare type GameObjectProperties<T> = {
    [P in keyof T]?: T[P];
};
export declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
