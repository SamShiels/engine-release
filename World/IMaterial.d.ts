import { IUniforms } from "../Renderer/WebGL/Material/Types";
export interface IMaterial {
    id: string;
    vertexShader: string;
    fragmentShader: string;
    uniforms: IUniforms;
}
