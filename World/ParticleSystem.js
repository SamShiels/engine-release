"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var GameObject_1 = require("./GameObject");
var Mesh_1 = require("./Mesh");
var Engine_1 = require("../Engine");
var ParticleSystem = (function (_super) {
    __extends(ParticleSystem, _super);
    function ParticleSystem() {
        var _this = _super.call(this) || this;
        _this._prefab = {};
        _this._spawnRate = 1;
        _this._spawnArea = { x: 1, y: 1 };
        _this._startVelocity = { x: 0, y: 0 };
        _this._startVelocityMin = { x: 0, y: 0 };
        _this._startVelocityMax = { x: 0, y: 0 };
        _this._lifeSpan = 1;
        _this._simulateInWorldSpace = false;
        _this._startSize = { x: 1, y: 1 };
        _this._endSize = { x: 1, y: 1 };
        _this._isRunning = false;
        _this._particles = [];
        _this._lastSpawn = 0;
        _this._update();
        return _this;
    }
    Object.defineProperty(ParticleSystem.prototype, "prefab", {
        get: function () {
            return this._prefab;
        },
        set: function (value) {
            this._prefab = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "spawnRate", {
        get: function () {
            return this._spawnRate;
        },
        set: function (value) {
            this._spawnRate = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "spawnArea", {
        get: function () {
            return this._spawnArea;
        },
        set: function (value) {
            this._spawnArea = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "startVelocity", {
        get: function () {
            return this._startVelocity;
        },
        set: function (value) {
            this._startVelocity = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "startVelocityMin", {
        get: function () {
            return this._startVelocityMin;
        },
        set: function (value) {
            this._startVelocityMin = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "startVelocityMax", {
        get: function () {
            return this._startVelocityMax;
        },
        set: function (value) {
            this._startVelocityMax = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "lifeSpan", {
        get: function () {
            return this._lifeSpan;
        },
        set: function (value) {
            this._lifeSpan = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "simulateInWorldSpace", {
        get: function () {
            return this._simulateInWorldSpace;
        },
        set: function (value) {
            this._simulateInWorldSpace = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "startSize", {
        get: function () {
            return this._startSize;
        },
        set: function (value) {
            this._startSize = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "endSize", {
        get: function () {
            return this._endSize;
        },
        set: function (value) {
            this._endSize = value;
        },
        enumerable: true,
        configurable: true
    });
    ParticleSystem.prototype._spawn = function () {
        if (!this._isRunning) {
            return;
        }
        var spawnPosition;
        var mesh;
        if (!this._simulateInWorldSpace) {
            mesh = this.instantiate(Mesh_1.Mesh, this._prefab);
            spawnPosition = { x: 0, y: 0 };
        }
        else {
            mesh = Engine_1.Engine.gameContainer.instantiate(Mesh_1.Mesh, this._prefab);
            var worldMat = this.getLocalToWorldMatrix();
            var xPos = worldMat[4];
            var yPos = worldMat[5];
            spawnPosition = { x: xPos, y: yPos };
        }
        mesh.position = { x: spawnPosition.x + this._position.x + Math.random() * this.spawnArea.x, y: spawnPosition.y + this._position.y + Math.random() * this.spawnArea.y };
        mesh.scale = { x: 1, y: 1 };
        var timeStamp = performance.now();
        var randomVelocityRangeX = this.startVelocityMax.x - this.startVelocityMin.x;
        var randomVelocityRangeY = this.startVelocityMax.y - this.startVelocityMin.y;
        var velocity = { x: this.startVelocityMin.x + Math.random() * randomVelocityRangeX, y: this.startVelocityMin.y + Math.random() * randomVelocityRangeY };
        var particle = {
            mesh: mesh,
            velocity: velocity,
            spawnTimestamp: timeStamp
        };
        this._particles.push(particle);
    };
    ParticleSystem.prototype._update = function () {
        var now = performance.now();
        if (now > this._lastSpawn + 1000 / this._spawnRate) {
            var spawnAmount = Math.floor(this.spawnRate / 60);
            for (var i = 0; i < Math.max(spawnAmount, 1); i++) {
                this._spawn();
            }
            this._lastSpawn = now;
        }
        var spliceIndices = [];
        for (var i = 0; i < this._particles.length; i++) {
            var particle = this._particles[i];
            particle.mesh.position.x += particle.velocity.x;
            particle.mesh.position.y += particle.velocity.y;
            var xScaleDiff = this._endSize.x - this._startSize.x;
            var yScaleDiff = this._endSize.y - this._startSize.y;
            particle.mesh.scale.x = this._startSize.x + xScaleDiff * (now - particle.spawnTimestamp) / 1000 * this._lifeSpan;
            particle.mesh.scale.y = this._startSize.y + yScaleDiff * (now - particle.spawnTimestamp) / 1000 * this._lifeSpan;
            if (now > particle.spawnTimestamp + this._lifeSpan * 1000) {
                spliceIndices.push(i);
            }
        }
        for (var i = 0; i < spliceIndices.length; i++) {
            if (spliceIndices[i] > this._particles.length - 1) {
                continue;
            }
            this._particles[spliceIndices[i]].mesh.destroy();
            this._particles.splice(spliceIndices[i], 1);
        }
        requestAnimationFrame(this._update.bind(this));
    };
    ParticleSystem.prototype.start = function () {
        this._isRunning = true;
    };
    ParticleSystem.prototype.stop = function () {
        this._isRunning = false;
    };
    ParticleSystem.prototype.isRunning = function () {
        return this._isRunning;
    };
    return ParticleSystem;
}(GameObject_1.GameObject));
exports.ParticleSystem = ParticleSystem;
//# sourceMappingURL=ParticleSystem.js.map