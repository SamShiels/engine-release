import { TextureFilter } from "../Renderer/WebGL/Texture/TextureFilter";
export interface ITexture {
    imageId: string;
    filter?: TextureFilter;
}
