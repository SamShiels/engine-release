import { KeyAction } from "./KeyAction";
import { IInput } from "./IInput";
import { IVector2 } from "../Utils/Vector2/IVector2";
export declare class Input implements IInput {
    private _canvas;
    private _document;
    private _pressedKeys;
    private _lastMousePosition;
    private _mousePosition;
    private _mouseMoveCallback;
    private _pointerLocked;
    get pointerLocked(): boolean;
    constructor(canvas: HTMLCanvasElement);
    private _lockChangeAlert;
    lockCursor(): void;
    unlockCursor(): void;
    getKeyPress(key: string): boolean;
    bindKeys(keys: string[], action: KeyAction, callback: (key: string) => void): void;
    getMousePosition(): IVector2;
    registerMouseMoveCallback(callback: (mouseDelta: IVector2) => void): void;
}
