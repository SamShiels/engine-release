"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyAction;
(function (KeyAction) {
    KeyAction["DOWN"] = "keydown";
    KeyAction["UP"] = "keyup";
})(KeyAction = exports.KeyAction || (exports.KeyAction = {}));
//# sourceMappingURL=KeyAction.js.map