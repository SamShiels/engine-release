import { KeyAction } from "./KeyAction";
import { IVector2 } from "../Utils/Vector2/IVector2";
export interface IInput {
    pointerLocked: boolean;
    bindKeys(keys: string[], action: KeyAction, callback: (key: string) => void): void;
    getKeyPress(key: string): boolean;
    getMousePosition(): IVector2;
    lockCursor(): void;
    unlockCursor(): void;
    registerMouseMoveCallback(callback: (mouseDelta: IVector2) => void): void;
}
