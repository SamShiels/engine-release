"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Vector2_1 = require("../Utils/Vector2/Vector2");
var Input = (function () {
    function Input(canvas) {
        var _this = this;
        this._pressedKeys = [];
        this._mousePosition = Vector2_1.Vector2.zero();
        this._pointerLocked = false;
        this._pressedKeys = [];
        this._mousePosition = { x: 0, y: 0 };
        this._canvas = canvas;
        var can = canvas;
        this._document = document;
        can.requestPointerLock = can.requestPointerLock || can.mozRequestPointerLock;
        this._document.exitPointerLock = this._document.exitPointerLock || this._document.mozExitPointerLock;
        this._canvas.addEventListener('pointerlockerror', function (event) {
            console.log('Error locking pointer');
        });
        canvas.addEventListener('mousemove', function (e) {
            var cRect = canvas.getBoundingClientRect();
            _this._mousePosition.x = e.clientX - cRect.left;
            _this._mousePosition.y = e.clientY - cRect.top;
            if (_this._mouseMoveCallback) {
                _this._mouseMoveCallback({ x: e.movementX, y: e.movementY });
            }
        });
        document.onmousedown = function (e) {
            var mouseButtonName = 'mouse' + e.button;
            if (_this._pressedKeys.includes(mouseButtonName) === false) {
                _this._pressedKeys.push(mouseButtonName);
            }
        };
        var mouseUp = function (e) {
            var mouseButtonName = 'mouse' + e.button;
            if (_this._pressedKeys.includes(mouseButtonName) === true) {
                _this._pressedKeys.splice(_this._pressedKeys.indexOf(mouseButtonName), 1);
            }
        };
        document.onmouseup = mouseUp;
        document.onmouseleave = mouseUp;
        document.addEventListener('keydown', function (event) {
            if (event.repeat === false) {
                if (_this._pressedKeys.includes(event.key) === false) {
                    _this._pressedKeys.push(event.key);
                }
            }
        });
        document.addEventListener('keyup', function (event) {
            if (event.repeat === false) {
                if (_this._pressedKeys.includes(event.key) === true) {
                    _this._pressedKeys.splice(_this._pressedKeys.indexOf(event.key), 1);
                }
            }
        });
        if ("onpointerlockchange" in document) {
            this._document.addEventListener('pointerlockchange', this._lockChangeAlert.bind(this), false);
        }
        else if ("onmozpointerlockchange" in document) {
            this._document.addEventListener('mozpointerlockchange', this._lockChangeAlert.bind(this), false);
        }
    }
    Object.defineProperty(Input.prototype, "pointerLocked", {
        get: function () {
            return this._pointerLocked;
        },
        enumerable: true,
        configurable: true
    });
    Input.prototype._lockChangeAlert = function () {
        if (this._document.pointerLockElement === this._canvas ||
            this._document.mozPointerLockElement === this._canvas) {
            this._pointerLocked = true;
        }
        else {
            this._pointerLocked = false;
        }
    };
    Input.prototype.lockCursor = function () {
        this._canvas.requestPointerLock();
    };
    Input.prototype.unlockCursor = function () {
        document.exitPointerLock();
    };
    Input.prototype.getKeyPress = function (key) {
        return this._pressedKeys.includes(key);
    };
    Input.prototype.bindKeys = function (keys, action, callback) {
        keys.forEach(function (key) {
            document.addEventListener(action, function (event) {
                if (event.key === key && event.repeat === false) {
                    callback(event.key);
                }
            });
        });
    };
    Input.prototype.getMousePosition = function () {
        return this._mousePosition;
    };
    Input.prototype.registerMouseMoveCallback = function (callback) {
        this._mouseMoveCallback = callback;
    };
    return Input;
}());
exports.Input = Input;
//# sourceMappingURL=Input.js.map