export declare enum KeyAction {
    DOWN = "keydown",
    UP = "keyup"
}
