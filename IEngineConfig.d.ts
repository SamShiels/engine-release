export interface IEngineConfig {
    canvasWidth: number;
    canvasHeight: number;
    fixedUpdateInterval?: number;
    textSmoothingEnabled?: boolean;
}
