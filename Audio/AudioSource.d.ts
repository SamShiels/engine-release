import { IAudioSource } from "./IAudioSource";
export declare class AudioSource implements IAudioSource {
    private _howl;
    protected _clip: string;
    get clip(): string;
    set clip(value: string);
    protected _volume: number;
    get volume(): number;
    set volume(value: number);
    protected _loop: boolean;
    get loop(): boolean;
    set loop(value: boolean);
    constructor(clip: string, volume?: number, loop?: boolean);
    private _createHowl;
    play(): void;
    pause(): void;
    stop(): void;
    destroy(): void;
}
