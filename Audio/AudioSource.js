"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var howler_1 = require("howler");
var AudioSource = (function () {
    function AudioSource(clip, volume, loop) {
        if (volume === void 0) { volume = 1.0; }
        if (loop === void 0) { loop = false; }
        this._clip = '';
        this._volume = 1;
        this._loop = false;
        this._clip = clip;
        this._volume = volume;
        this._loop = loop;
        this._createHowl();
    }
    Object.defineProperty(AudioSource.prototype, "clip", {
        get: function () {
            return this._clip;
        },
        set: function (value) {
            this._clip = value;
            this._createHowl();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AudioSource.prototype, "volume", {
        get: function () {
            return this._volume;
        },
        set: function (value) {
            this._volume = value;
            this._createHowl();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AudioSource.prototype, "loop", {
        get: function () {
            return this._loop;
        },
        set: function (value) {
            this._loop = value;
            this._createHowl();
        },
        enumerable: true,
        configurable: true
    });
    AudioSource.prototype._createHowl = function () {
        this._howl = new howler_1.Howl({
            src: [this._clip],
            volume: this._volume,
            loop: this._loop,
            preload: true
        });
    };
    AudioSource.prototype.play = function () {
        if (!this._howl) {
            this._createHowl();
        }
        this._howl.play();
    };
    AudioSource.prototype.pause = function () {
        this._howl.pause();
    };
    AudioSource.prototype.stop = function () {
        this._howl.stop();
    };
    AudioSource.prototype.destroy = function () {
        this._howl = null;
        this._clip = null;
        this._volume = null;
        this._loop = null;
    };
    return AudioSource;
}());
exports.AudioSource = AudioSource;
//# sourceMappingURL=AudioSource.js.map