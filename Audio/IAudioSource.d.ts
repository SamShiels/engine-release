export interface IAudioSource {
    clip: string;
    volume: number;
    loop: boolean;
    play(): void;
    pause(): void;
    stop(): void;
    destroy(): void;
}
