export interface ISystem {
    renderLoop(time?: number): void;
    fixedLoop(): void;
}
