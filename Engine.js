"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BehaviourManager_1 = require("./Behaviours/BehaviourManager");
var Input_1 = require("./Input/Input");
var Physics_1 = require("./Physics/Physics");
var CanvasRenderer_1 = require("./Renderer/Canvas/CanvasRenderer");
var ServerUIRenderer_1 = require("./Renderer/Canvas/ServerUIRenderer");
var ServerRenderer_1 = require("./Renderer/WebGL/Renderer/ServerRenderer");
var WebGLRenderer_1 = require("./Renderer/WebGL/Renderer/WebGLRenderer");
var GameObject_1 = require("./World/GameObject");
var Engine = (function () {
    function Engine() {
    }
    Object.defineProperty(Engine, "canvasWidth", {
        get: function () {
            return this._canvasWidth;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Engine, "canvasHeight", {
        get: function () {
            return this._canvasHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Engine, "gameContainer", {
        get: function () {
            return this._gameContainer;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Engine, "textContainer", {
        get: function () {
            return this._textContainer;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Engine, "input", {
        get: function () {
            return this._input;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Engine, "physics", {
        get: function () {
            return this._physics;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Engine, "timeSinceInit", {
        get: function () {
            return this._timeSinceInit;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Engine, "deltaTime", {
        get: function () {
            return this._deltaTime;
        },
        enumerable: true,
        configurable: true
    });
    Engine.init = function (config) {
        this._gameContainer = new GameObject_1.GameObject();
        this._textContainer = new GameObject_1.GameObject();
        this._canvasWidth = config.canvasWidth;
        this._canvasHeight = config.canvasHeight;
        this._createEngineSystems(config);
        var isServer = false;
        if (!isServer) {
            this._lastUpdate = Date.now();
            this._gameLoop();
            window.Engine = this;
        }
        this._fixedLoop(config.fixedUpdateInterval ? config.fixedUpdateInterval : 15);
    };
    Engine.enterFullscreen = function () {
    };
    Engine.retrieveSystem = function (id) {
        return this._systemMap.get(id);
    };
    Engine._gameLoop = function () {
        var now = Date.now();
        this._deltaTime = (now - this._lastUpdate) / 1000.0;
        if (this._deltaTime > this.MAX_DELTA_TIME) {
            this._deltaTime = this.MAX_DELTA_TIME;
        }
        this._timeSinceInit = this._timeSinceInit + this._deltaTime;
        this._systemMap.forEach(function (system) {
            system.renderLoop();
        });
        this._lastUpdate = now;
        requestAnimationFrame(this._gameLoop.bind(this));
    };
    Engine._fixedLoop = function (tick) {
        var _this = this;
        setInterval(function () {
            _this._systemMap.forEach(function (system) {
                system.fixedLoop();
            });
        }, tick);
    };
    Engine._createEngineSystems = function (config) {
        var isServer = false;
        if (!isServer) {
            var webgl = document.getElementById("gl");
            var canvas = document.getElementById("canvas2d");
            var glContext = webgl.getContext('webgl');
            var canvasContext = canvas.getContext('2d');
            webgl.width = config.canvasWidth;
            webgl.height = config.canvasHeight;
            canvas.width = config.canvasWidth;
            canvas.height = config.canvasHeight;
            this._input = new Input_1.Input(canvas);
            this._systemMap.set('renderer', new WebGLRenderer_1.WebGLRenderer(glContext));
            this._systemMap.set('textRenderer', new CanvasRenderer_1.CanvasRenderer(canvasContext, config.textSmoothingEnabled));
            this.resize(this._canvasWidth, this._canvasHeight);
            this._gameContainer.width = glContext.canvas.width;
            this._gameContainer.height = glContext.canvas.height;
            this._textContainer.width = canvasContext.canvas.width;
            this._textContainer.height = canvasContext.canvas.height;
        }
        else {
            this._systemMap.set('renderer', new ServerRenderer_1.ServerRenderer());
            this._systemMap.set('textRenderer', new ServerUIRenderer_1.ServerUIRenderer());
        }
        var physics = new Physics_1.Physics();
        this._physics = physics;
        this._systemMap.set('physics', physics);
        this._systemMap.set('behaviourManager', new BehaviourManager_1.BehaviourManager());
    };
    Engine.resize = function (canvasWidth, canvasHeight) {
        this._canvasWidth = canvasWidth;
        this._canvasHeight = canvasHeight;
        var webgl = document.getElementById("gl");
        var canvas = document.getElementById("canvas2d");
        var renderer = this.retrieveSystem('renderer');
        webgl.width = canvasWidth;
        webgl.height = canvasHeight;
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        renderer.canvasWidth = canvasWidth;
        renderer.canvasHeight = canvasHeight;
    };
    Engine._canvasWidth = 0;
    Engine._canvasHeight = 0;
    Engine.MAX_DELTA_TIME = 1000;
    Engine._timeSinceInit = 0;
    Engine._deltaTime = 0;
    Engine._systemMap = new Map();
    return Engine;
}());
exports.Engine = Engine;
//# sourceMappingURL=Engine.js.map