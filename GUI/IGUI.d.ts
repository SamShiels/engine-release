import { IGameObject } from "../World/IGameObject";
import { IColor } from "../Utils/IColor";
import { IVector2 } from "../Utils/Vector2/IVector2";
export interface IGUI extends IGameObject {
    origin: IVector2;
    alpha: number;
    color: IColor;
    render(context: CanvasRenderingContext2D): void;
    destroy(): void;
}
