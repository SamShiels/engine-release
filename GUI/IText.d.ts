import { IGUI } from "./IGUI";
export interface IText extends IGUI {
    text: string;
    fontFace: string;
    fontSize: number;
    textAlign: CanvasTextAlign;
}
