"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var GUI_1 = require("./GUI");
var Text = (function (_super) {
    __extends(Text, _super);
    function Text() {
        var _this = _super.call(this) || this;
        _this._text = '';
        _this._fontFace = '';
        _this._fontSize = 0;
        _this._textAlign = 'left';
        return _this;
    }
    Object.defineProperty(Text.prototype, "text", {
        get: function () {
            return this._text;
        },
        set: function (value) {
            this._text = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Text.prototype, "fontFace", {
        get: function () {
            return this._fontFace;
        },
        set: function (value) {
            this._fontFace = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Text.prototype, "fontSize", {
        get: function () {
            return this._fontSize;
        },
        set: function (value) {
            this._fontSize = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Text.prototype, "textAlign", {
        get: function () {
            return this._textAlign;
        },
        set: function (value) {
            this._textAlign = value;
        },
        enumerable: true,
        configurable: true
    });
    Text.prototype.render = function (context) {
        if (this._enabled === false || this._enabledInHierarchy === false) {
            return;
        }
        context.font = this._fontSize + "px " + this._fontFace;
        var color = this._color;
        context.fillStyle = "rgba(" + color.r + ", " + color.g + ", " + color.b + ", " + this._alpha;
        context.textAlign = this._textAlign;
        var worldMat = this.getLocalToWorldMatrix();
        var xPos = worldMat[4];
        var yPos = worldMat[5];
        context.setTransform(1, 0, 0, 1, xPos, yPos);
        context.rotate(Math.atan2(worldMat[1], worldMat[0]));
        context.fillText(this._text, 0, 0);
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.globalCompositeOperation = 'source-over';
    };
    Text.prototype.destroy = function () {
        this._text = null;
        this._fontFace = null;
        this._fontSize = null;
        this._textAlign = null;
    };
    return Text;
}(GUI_1.GUI));
exports.Text = Text;
//# sourceMappingURL=Text.js.map