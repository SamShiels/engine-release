import { GameObject } from "../World/GameObject";
import { IGUI } from "./IGUI";
import { IColor } from "../Utils/IColor";
import { IVector2 } from "../Utils/Vector2/IVector2";
export declare abstract class GUI extends GameObject implements IGUI {
    private _textRenderer;
    protected _origin: IVector2;
    get origin(): IVector2;
    set origin(value: IVector2);
    protected _color: IColor;
    get color(): IColor;
    set color(value: IColor);
    protected _alpha: number;
    get alpha(): number;
    set alpha(value: number);
    constructor();
    protected _convertRgbToHex(color: IColor): string;
    abstract render(context: CanvasRenderingContext2D): void;
    destroy(): void;
}
