import { IText } from "./IText";
import { GUI } from "./GUI";
export declare class Text extends GUI implements IText {
    private _text;
    get text(): string;
    set text(value: string);
    private _fontFace;
    get fontFace(): string;
    set fontFace(value: string);
    private _fontSize;
    get fontSize(): number;
    set fontSize(value: number);
    private _textAlign;
    get textAlign(): CanvasTextAlign;
    set textAlign(value: CanvasTextAlign);
    constructor();
    render(context: CanvasRenderingContext2D): void;
    destroy(): void;
}
