"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var GameObject_1 = require("../World/GameObject");
var Engine_1 = require("../Engine");
var GUI = (function (_super) {
    __extends(GUI, _super);
    function GUI() {
        var _this = _super.call(this) || this;
        _this._origin = { x: 0.5, y: 0.5 };
        _this._color = { r: 255, g: 255, b: 255 };
        _this._alpha = 1;
        _this._textRenderer = Engine_1.Engine.retrieveSystem('textRenderer');
        _this._textRenderer.addControl(_this);
        return _this;
    }
    Object.defineProperty(GUI.prototype, "origin", {
        get: function () {
            return this._origin;
        },
        set: function (value) {
            this._origin = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GUI.prototype, "color", {
        get: function () {
            return this._color;
        },
        set: function (value) {
            this._color = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GUI.prototype, "alpha", {
        get: function () {
            return this._alpha;
        },
        set: function (value) {
            this._alpha = value;
        },
        enumerable: true,
        configurable: true
    });
    GUI.prototype._convertRgbToHex = function (color) {
        var hexString = '#';
        var redHex = color.r.toString(16);
        hexString += redHex.length === 1 ? '0' + redHex : redHex;
        var greenHex = color.g.toString(16);
        hexString += greenHex.length === 1 ? '0' + greenHex : greenHex;
        var blueHex = color.b.toString(16);
        hexString += blueHex.length === 1 ? '0' + blueHex : blueHex;
        return hexString;
    };
    GUI.prototype.destroy = function () {
        this._textRenderer.removeControl(this);
        this._origin = null;
        this._color = null;
        this._alpha = null;
        _super.prototype.destroy.call(this);
    };
    return GUI;
}(GameObject_1.GameObject));
exports.GUI = GUI;
//# sourceMappingURL=GUI.js.map