"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BehaviourManager = (function () {
    function BehaviourManager() {
        this._behaviours = [];
    }
    BehaviourManager.prototype.renderLoop = function () {
        for (var i = 0; i < this._behaviours.length; i++) {
            this._behaviours[i].update();
        }
    };
    BehaviourManager.prototype.fixedLoop = function () {
        for (var i = 0; i < this._behaviours.length; i++) {
            this._behaviours[i].fixedUpdate();
        }
    };
    BehaviourManager.prototype.addBehaviour = function (behaviour) {
        this._behaviours.push(behaviour);
    };
    BehaviourManager.prototype.removeBehaviour = function (behaviour) {
        this._behaviours.splice(this._behaviours.indexOf(behaviour), 1);
    };
    return BehaviourManager;
}());
exports.BehaviourManager = BehaviourManager;
//# sourceMappingURL=BehaviourManager.js.map