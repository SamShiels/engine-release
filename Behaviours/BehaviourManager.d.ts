import { ISystem } from "../ISystem";
import { IBehaviour } from "./IBehaviour";
import { IBehaviourManager } from "./IBehaviourManager";
export declare class BehaviourManager implements IBehaviourManager, ISystem {
    private _behaviours;
    constructor();
    renderLoop(): void;
    fixedLoop(): void;
    addBehaviour(behaviour: IBehaviour): void;
    removeBehaviour(behaviour: IBehaviour): void;
}
