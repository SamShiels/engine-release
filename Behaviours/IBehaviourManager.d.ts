import { IBehaviour } from "./IBehaviour";
export interface IBehaviourManager {
    addBehaviour(behaviour: IBehaviour): void;
    removeBehaviour(behaviour: IBehaviour): void;
}
