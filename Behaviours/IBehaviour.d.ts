export interface IBehaviour {
    start(): void;
    update(): void;
    fixedUpdate(): void;
    networkUpdate(): void;
}
