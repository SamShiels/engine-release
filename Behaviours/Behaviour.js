"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Engine_1 = require("../Engine");
var Behaviour = (function () {
    function Behaviour() {
        this._behaviourManager = Engine_1.Engine.retrieveSystem('behaviourManager');
        this._behaviourManager.addBehaviour(this);
        this.start();
    }
    Behaviour.prototype.start = function () {
    };
    Behaviour.prototype.update = function () {
    };
    Behaviour.prototype.fixedUpdate = function () {
    };
    Behaviour.prototype.networkUpdate = function () {
    };
    Behaviour.prototype.destroy = function () {
        this._behaviourManager.removeBehaviour(this);
        this._behaviourManager = null;
    };
    return Behaviour;
}());
exports.Behaviour = Behaviour;
//# sourceMappingURL=Behaviour.js.map