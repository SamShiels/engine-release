import { IBehaviour } from "./IBehaviour";
export declare abstract class Behaviour implements IBehaviour {
    private _behaviourManager;
    constructor();
    start(): void;
    update(): void;
    fixedUpdate(): void;
    networkUpdate(): void;
    destroy(): void;
}
