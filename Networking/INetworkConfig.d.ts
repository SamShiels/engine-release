import { NetworkMode } from "./NetworkMode";
import socketIO from 'socket.io';
export interface INetworkConfig {
    networkMode: NetworkMode;
    httpServer?: socketIO.Server;
    tick: number;
}
