export declare enum NetworkMode {
    Offline = "Offline",
    Server = "Server",
    Client = "Client"
}
