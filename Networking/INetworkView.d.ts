import { INetworkPlayer } from "./INetworkPlayer";
export interface INetworkView {
    owner: INetworkPlayer;
}
