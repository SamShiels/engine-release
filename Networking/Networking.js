"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var socket_io_client_1 = __importDefault(require("socket.io-client"));
var NetworkMode_1 = require("./NetworkMode");
var Networking = (function () {
    function Networking(mode) {
        this._netMode = NetworkMode_1.NetworkMode.Client;
        this._connectedPlayers = [];
        this._connectedSockets = [];
        this._netMode = mode;
    }
    Object.defineProperty(Networking.prototype, "networkMode", {
        get: function () {
            return this._netMode;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Networking.prototype, "server", {
        get: function () {
            return this._server;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Networking.prototype, "player", {
        get: function () {
            return this._player;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Networking.prototype, "connectedPlayers", {
        get: function () {
            return this._connectedPlayers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Networking.prototype, "clientSocket", {
        get: function () {
            return this._socket;
        },
        enumerable: true,
        configurable: true
    });
    Networking.prototype.initializeServer = function (server, tick) {
        var _this = this;
        this._server = server;
        this._server.on('connection', function (socket) {
            _this._connectedSockets.push(socket);
            _this._connectedPlayers.push({
                id: socket.id
            });
            console.log('Player connected with id: ' + socket.id);
            socket.on('disconnect', function () {
                console.log('Player disconnected with id: ' + socket.id);
                _this._connectedSockets.splice(_this._connectedSockets.indexOf(socket));
                var player = _this._connectedPlayers.find(function (player) { return player.id === socket.id; });
                if (player) {
                    _this._connectedPlayers.splice(_this._connectedPlayers.indexOf(player), 1);
                }
            });
        });
        setInterval(function () {
            for (var i = 0; i < _this._connectedPlayers.length; i++) {
                _this._connectedSockets[i].emit('update');
            }
        }, 1000 / tick);
    };
    Networking.prototype.connect = function (ipAddress, port) {
        if (this._netMode === NetworkMode_1.NetworkMode.Server || this._netMode === NetworkMode_1.NetworkMode.Offline) {
            return;
        }
        this._socket = socket_io_client_1.default.connect(ipAddress + ":" + port);
        this._player = {
            id: this._socket.id,
        };
        this._socket.on('connect', function () {
            console.log('connected');
        });
        this._socket.on('disconnect', function () {
            console.log('disconnect');
        });
        this._socket.on('update', function () {
            console.log('update');
        });
    };
    Networking.prototype.disconnect = function () {
        if (this._socket === undefined || this._netMode === NetworkMode_1.NetworkMode.Server || this._netMode === NetworkMode_1.NetworkMode.Offline) {
            return;
        }
        this._socket.disconnect();
    };
    Networking.prototype.createNetworkView = function (owner, gameObject) {
    };
    Networking.prototype.renderLoop = function () {
    };
    Networking.prototype.fixedLoop = function () {
    };
    return Networking;
}());
exports.Networking = Networking;
//# sourceMappingURL=Networking.js.map