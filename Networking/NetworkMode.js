"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NetworkMode;
(function (NetworkMode) {
    NetworkMode["Offline"] = "Offline";
    NetworkMode["Server"] = "Server";
    NetworkMode["Client"] = "Client";
})(NetworkMode = exports.NetworkMode || (exports.NetworkMode = {}));
//# sourceMappingURL=NetworkMode.js.map