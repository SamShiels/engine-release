/// <reference types="socket.io-client" />
import * as socketServer from 'socket.io';
import { NetworkMode } from "./NetworkMode";
import { INetworkPlayer } from './INetworkPlayer';
export interface INetworking {
    networkMode: NetworkMode;
    server: socketServer.Server | undefined;
    player: INetworkPlayer | undefined;
    clientSocket: SocketIOClient.Socket | undefined;
    initializeServer(server: socketServer.Server, tick: number): void;
    connect(ipAddress: string, port: string): void;
    disconnect(): void;
}
