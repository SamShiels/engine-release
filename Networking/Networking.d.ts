/// <reference types="socket.io-client" />
import { INetworking } from "./INetworking";
import * as socketServer from 'socket.io';
import { IGameObject } from "../World/IGameObject";
import { INetworkPlayer } from "./INetworkPlayer";
import { NetworkMode } from "./NetworkMode";
import { ISystem } from "../ISystem";
export declare class Networking implements INetworking, ISystem {
    private _netMode;
    get networkMode(): NetworkMode;
    private _server;
    get server(): socketServer.Server | null;
    private _player;
    get player(): INetworkPlayer | null;
    private _connectedPlayers;
    get connectedPlayers(): INetworkPlayer[];
    private _socket;
    get clientSocket(): SocketIOClient.Socket | null;
    private _connectedSockets;
    constructor(mode: NetworkMode);
    initializeServer(server: socketServer.Server, tick: number): void;
    connect(ipAddress: string, port: string): void;
    disconnect(): void;
    createNetworkView(owner: INetworkPlayer, gameObject: IGameObject): void;
    renderLoop(): void;
    fixedLoop(): void;
}
